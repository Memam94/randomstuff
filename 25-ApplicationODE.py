#Ex[8.4] - Vertical motion
# Using Eurler method with shooting method by start with a guess

import numpy as np
import matplotlib.pyplot as plt

g, N = 9.81, 1000                                # Gravity acc and number of trials 
v0 = 2                                           # velocity guess

t = np.linspace(0, 3, N)                         # uniform random N values of t
h = t[1] - t[0]                                  # The step depend on the number of trials
xis, vis = np.zeros(N), np.zeros(N)              # Empty array of position and velocity
 

def f(v0,t): return v0 - g*t                     # Define the function

while xis[N-1] <= 10 and v0 < 40:                # as the position less than the real value with upper limit of the velocity guess
    vis[0] = v0                                  # start with guess ( set the velocity start with initial velocity)
    for i in range(N-1):                         
        vis[i+1] = vis[i] - g*h                  # Euler method
        xis[i+1] = xis[i] + f(v0,t[i])*h
    v0 = v0 + 0.1                                # Update the velocity with the step
    
print("Final Position = " ,xis[N-1])             # Final position
print("Initial Velocity = ", vis[0])             # Initial velocity
plt.plot(t,xis,"r-", label='distance(m)')
plt.plot(t,vis,"b-", label='velocity(m/s)')
plt.xlabel('Time')
plt.axis([0, 3, -15, 20])
plt.legend()                                     
plt.grid()
plt.show()