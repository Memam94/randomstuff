#Ex[2.2] - Read from file
#USing selection sort as the easiest way of sorting 

def selsort(a):
    so = []                                   #Empty list to store the minimum each time
    for i in range(0,len(a),1):               #The counter will act on whole list with step 1
        mi = a.index(min(a))                  #Find the index of the minimum number
        a[mi],a[0] = a[0], a[mi]                #Swap the min value with the first values
        so.append(a[0])                       #Insert the first value in the sorted list (so)
        a.remove(a[0])                        #Remove the first value from the original list then REPEAT
    print("this is selection sorting",so)

f = open("illustris3_135.dat","r")
lines = f.readlines()
ID = []

for i in lines[1:]: 
    a = i.split() 
    ID.append(float(a[0]))

selsort(ID)                                    #Sort