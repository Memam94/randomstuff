#Ex[7.1.1] - Trapezoidal method [ SIS ]
# Divide the area under the curve into small trapezoids( = n ) of height h

C = 3e18                            # The constant = 2(v^2)/G

def y(x):                           # Define the applied formula 
    return (C) 

def trap(a, b, n):                  # Define the intial and final x values
    h = (b - a) / n                 # Height of each coloumn
    s = (y(a) + y(b))               # The constant integration part 
    i = 1                           # trapezoids loop counters
    
    while i < n:                    # The summation part
        s = s + 2 * y ( a + i * h )    # Summation formula
        i += 1
    return (( h / 2 ) * s )         # Return the constant then add the new part over the loop

I = trap(0,3.08e17,100)  
print("mass in kg = ",I)

#Ex[7.1.2] - Trapezoidal method [ NFW ]
r1, r2, p = 1e8, 10, 3.141592
C = 3e18                     
def y(x): 
    return ((4*p*(x**2)*r1)/((x/r2)*((1+x/r2)**2))) 
def trap(a, b, n): 
    h = (b - a) / n 
    s = (y(a) + y(b)) 
    i = 1
    while i < n: 
        s+= 2*y(a+i*h) 
        i+= 1
    return (( h / 2 ) * s) 
I = trap(1e-30,1e3,1000)  
print("mass in kg = ",I)