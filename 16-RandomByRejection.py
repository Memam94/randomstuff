#Ex[6.3] - Rejection
#generate randon number -> find its pdf whatever inside (accepted) the function y range or outside (rejected)
# we compare nonuniform generated x with uniform (theoritical) generated y

import random
import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import norm

acceptedx, acceptedy, rejected = [], [], []        # Lists to store values

for i in range(10000):                             # No. of random numbers generated
    x = random.uniform(-10, 10)                    # generate two random numbers
    y = random.uniform(0,1)                         
    
    if y < norm.pdf(x, 0, 2):                          # check if it is belong or not
        acceptedx.append(x)                            # Store the accepted random numbers
        acceptedy.append(y)
    
gauss = np.linspace(-10, 10, 100000)               # Theoritical gauss
gnorm = norm.pdf(gauss,0,2)

plt.plot(gauss, gnorm, "r-", label='Theoritical Gaussian')               # Plot theoritical gauss
plt.plot(acceptedx, acceptedy, "bo", label='Rejection Sampling')               # Plot accepted values
plt.legend()
plt.grid()
plt.show()