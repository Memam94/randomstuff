#EX.[8.2] - Euler method
import numpy as np
import matplotlib.pyplot as plt

G, m1, m2 = 1, 1, 1
t0, t =0, 300
N, h = 30000, 0.01
x1, y1, vx1, vy1 = [1.0], [1.0], -0.5, 0.0     #initial values
x2, y2, vx2, vy2 = [-1.0], [-1.0], 0.5, 0.0

# define acceleration
def ax(x1,x2,y1,y2): return -(x1-x2) / (((x1-x2)**2+(y1-y2)**2)**1.5)
def ay(x1,x2,y1,y2): return -(y1-y2) / (((x1-x2)**2+(y1-y2)**2)**1.5)


for i in range(N):
    a_x = ax(x1[i],x2[i],y1[i],y2[i])   # Compute new acceleration
    a_y = ay(x1[i],x2[i],y1[i],y2[i])
    
    px1 = x1[i] + h * vx1               # New position (p) of x and y
    py1 = y1[i] + h * vy1
    px2 = x2[i] + h * vx2
    py2 = y2[i] + h * vy2

    vx1 = vx1 + h * a_x                 # Update the velociticies
    vy1 = vy1 + h * a_y
    vx2 = vx2 - h * a_x
    vy2 = vy2 - h * a_y
    
    x1.append(px1)                      # Store the results
    y1.append(py1)
    x2.append(px2)
    y2.append(py2)
                     
plt.plot(x1, y1, "r" , label='Body 1') #Euler method
plt.plot(x2, y2, "b", label='Body 2') 
plt.axis([-10, 10, -5, 5])
plt.xlabel('x',fontsize=10)
plt.ylabel('y',fontsize=10)
plt.legend()
plt.show()

