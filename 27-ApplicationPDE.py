#Ex[9.2] - Diffusion Equation
# Solve the second order PDE using forward-time centered-space (FTCS)
# Solve the spatial part using regular second order numerical derivative
# Solve the result as regular ODE using Euler method

import numpy as np
import scipy.integrate as sci
import matplotlib.pyplot as plt

N = 100                               # No. of sample
L, D = 1, 4.25e-2                     # length and steel cofficient
a = L / N                             # grid step
h = 0.01                              # Euler step

#Create Temprature SteelT matrix - length matrix x - change matrix phit
steelT = 293.0 * np.ones(N) 
steelT[0], steelT[-1] = 273, 323 
x = np.linspace(0, L, N) 
phit = np.zeros(N)

#Numerical differntiation
def phi(x, t):
    for i in range(1, N-1):
        phit[i] = (D / a**2) * (x[i+1] + x[i-1] - 2 * x[i]) 
    return phit

#Integrate the time
time = [0.01, 0.1, 1, 10]
colors = ["b", "r", "g", "m"]
Temp = sci.odeint(phi, steelT, time)    
for i in range(0, len(time)):
    plt.plot(x, Temp[i],colors[i], label= time[i])
    plt.xlabel('x(cm)')
    plt.ylabel('T(k)') 

plt.legend()
plt.grid()
plt.show()