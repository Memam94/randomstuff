#Ex [6.1] - Inverse random sample (initial mass)
# Generate equal probability numbers within a range then apply a transformation

import numpy as np
import random
import matplotlib.pyplot as plt

mass, L = [], []
N = 1000000

for i in range(N):
    y =random.uniform(0.1,150)    # Generate randm numbers within the range
    x = y**(-2.3)                  # Apply the Transformation
    dN = abs(N/(np.log(x) * x))
    mass.append(x)               # Store results
    L.append(dN)

plt.plot(mass,L, 'b--')
#plt.axis([0, 200, 0, 200])
plt.xlabel('$M_\u2609$', fontsize=10)
plt.ylabel('dN/dM ($(M_\u2609)^{-1}$', fontsize=10)
plt.yscale('log')
plt.grid()
plt.show()

