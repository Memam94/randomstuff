#Ex[7.4] - Maan value method to calculate the mass

import numpy as np
from scipy import random

sum, a, b, c, r1, r2, p, N = 0, 1e-30, 1e3, 3e18, 1e8, 10, np.pi, 10000
xrand = random.uniform(a,b,N) 

def f(x):
    return ((4*p*(x**2)*r1)/((x/r2)*((1+x/r2)**2)))

for i in range(len(xrand)):
    sum += f(xrand[i])
I = (b-a)/N*sum

print("Mass = ",I)