#Ex[1.3] = Data extraction

import re
import os
import numpy as np
import matplotlib.pyplot as plt 

f = open('starlab_output.txt', 'r')
a = f.read()
r = re.findall(" kira_rhalf = (\S+)", a)
time = re.findall("  t  =  (\S+)", a)
for i in range(len(r)):
    r[i] = float(r[i])
t = np.linspace(1.,len(r),len(r))

plt.plot(t,r)
plt.grid()
plt.show()

