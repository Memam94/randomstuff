#Ex[9.1] - Laplace Equation
# Solve second order PDE using Boundary-value problem with finite-difference methods (FDMs)
# Solve two derivative second order derivative which lead to solve them as regular two linear equation
# Solve them using Gauss-Seidel method by assuming a guess

import numpy as np
import matplotlib.pyplot as plt

m = 100                                    # Diminsion of matrix

x = y = np.zeros((m,m))                    # Create x and y matrix (grid) - First guess
phi = np.exp(x,y)                          # take 0 exponent = 1
phi[0:] = 1                                # Set boundaries
phi[1:] = 0                                # set other elements 

w = 0.9                                    # Relaxation coefficient 
for k in range(300):                       # No. of iteration 
    for i in range(1, m-1):                # Calculate phi aside from the boundary values of y matrix 
        for j in range(1, m-1):            # Calculate phi aside from the boundary values of x matrix 
            phi[i, j] = (0.25*(1+w)) * (phi[i+1][j] + phi[i-1][j] + phi[i][j+1] + phi[i][j-1]) - w * phi[i][j]      
            # Apply x and y matrix to calcuate phi 
    R = phi 				   
    # store matrix to plot 

plt.imshow(R)
plt.gray()
plt.show()