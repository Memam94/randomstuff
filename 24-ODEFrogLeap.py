#Ex[8.3] - FrogLeap
# Similar to Euler's method but evaluated in between a time-step
# Second order method    (update the value two times)

import math
import matplotlib.pyplot as plt

G, m1, m2 = 1, 1, 1
t0, t, h = 0, 300, 0.01
N = (t-t0)/h
x1, y1, vx1, vy1 = [1.0], [1.0], -0.5, 0.0     #initial values
x2, y2, vx2, vy2 = [-1.0], [-1.0], 0.5, 0.0

def ax(x1,x2,y1,y2): return -(x1-x2) / (((x1-x2)**2+(y1-y2)**2)**1.5)
def ay(x1,x2,y1,y2): return -(y1-y2) / (((x1-x2)**2+(y1-y2)**2)**1.5)

for i in range(int(N)):        
    a_x = ax(x1[i],x2[i],y1[i],y2[i])   # acceleration a(t)
    a_y = ay(x1[i],x2[i],y1[i],y2[i])
    
    vx1 = vx1 + (h/2) * a_x         # velociticies at half v(t+h/2)
    vy1 = vy1 + (h/2) * a_y
    vx2 = vx2 - (h/2) * a_x
    vy2 = vy2 - (h/2) * a_y
    
    px1 = x1[i] + h * vx1           #  position x(t + h)
    py1 = y1[i] + h * vy1
    px2 = x2[i] + h * vx2 
    py2 = y2[i] + h * vy2 

    a_xx = ax(px1,px2,py1,py2)      #  acceleration a(t+h)
    a_yy = ay(px1,px2,py1,py2)
    
    vx1 = vx1 + (h/2) * a_xx        #  velociticies at full v(t+h)
    vy1 = vy1 + (h/2) * a_yy
    vx2 = vx2 - (h/2) * a_xx  
    vy2 = vy2 - (h/2) * a_yy
    
    x1.append(px1)                      # Store the results
    y1.append(py1)
    x2.append(px2)
    y2.append(py2)
        
plt.plot(x1, y1,"-b", label='Body A')
plt.plot(x2, y2,"-r", label='Body B')
plt.axis([-10, 10, -5, 5])
plt.xlabel("x")
plt.ylabel("y")
plt.legend() 
plt.grid()
plt.show()
    
