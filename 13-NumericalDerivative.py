#Ex[5.1] - Numerical derivative 
# implement Newton-Raphson with finding the derivative

import numpy as np

def f(x):
    return F - x + ecc*np.sin(x)       # Define the function

def nr(x):
    it, h = 1e-6, 0.01                            # Accuracy needed, and Derivative step
    for i in range(100):                          # No. of trials
        df = ((f(x+h) - f(x-h))/(2*h))            # central difference
        xnew = x - (f(x)/df)                      # Refine x value
        if abs(xnew - x) < it: break              # Check accuracy difference
        x = xnew                                  # Reset x value 
    print("Using NR and centeral diff    ",x)

ecc = float(input("ecc: "))
F = float(input("F: "))
x = nr(3)                       # first guess 3