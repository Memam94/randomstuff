#Ex[1.2] and [1.4] - Comoving and luminosity distances and Plotting
import matplotlib.pyplot as plt
import scipy.integrate as integrate

def Codistance(z):
	H0=67.2
	convert=1e5/3.086e24*3.1536e7*1e9 
	OmegaM,OmegaL=0.2726,0.7274
	integrand= lambda x: 1/((OmegaM*(1+x)**3+OmegaL)**0.5)     # The integration
	integ=integrate.quad(integrand,0,z)                        # Compute the integration
	Codis = integ[0]                                           # Convert 
	Codis/=(H0*convert)
	return Codis

Codis,Lodis,redsh = [], [], []
redsh=[]
z=0
while(z <= 10):
    Codis.append(Codistance(z))
    redsh.append(z)
    Lodis.append((1+z)*Codistance(z))
    if (z==9): break
    z+=1

plt.plot(redsh,Codis, 'r-', label='Comoving')
plt.plot(redsh,Lodis, 'b-', label='Luminosity')
plt.xlabel('Redshift', fontsize=10)
plt.ylabel('Distance', fontsize=10)
plt.legend()
plt.grid()

plt.show()