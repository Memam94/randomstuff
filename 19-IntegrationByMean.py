#Ex[7.3] - Mean Method
#find the function average by calculate the average of generated random numbers

import numpy as np
from scipy import random
import matplotlib.pyplot as plt
a , b, I_tot = 0, 2, []

N = [1000, 5000, 10000, 50000, 100000, 500000, 1000000, 5000000]

def f(x):                                    # Define the function
    return np.sin(1/x*(2-x))**2 

for j in range(int(len(N))):                      # Choose number of random numbers generated
    sum = 0
    for i in range(N[j]):                    
        xrand = np.random.rand()             # generate uniform N random number
        sum += f(xrand)                      # apply on function and add to the summ
        I = ((b-a)/N[j])*sum                 # Find the integration
    I_tot.append(I)
    
plt.plot(N,I_tot, color='b', marker='o')
plt.xlabel('N', fontsize=10)
plt.xscale('log')
plt.ylabel('Integration value', fontsize=10)
plt.grid()

plt.show()