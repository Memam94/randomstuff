#Ex[4.1.1] - Relaxation
#Guess the x value, reset the value till reach the accuracy needed
#should take x = f(x) form

import numpy as np

def relax(x,x0):  
    it = 1e-6                       #accuracy needed
    while(abs(x - x0) > it):        #check the difference
        x0 = x                      #Reset x0 value
        x = F + ecc*np.sin(x)       #Re-calculate x value from the guess
    print("Relaxation    ", x)
ecc = float(input("ecc: "))
F = float(input("F: "))
relax(1, 10)                        #Upper and lower guess

#Ex[4.1.2] - Bisection
#guess upper and lower limit -> find mid -> check the position -> repeat at smaller interval

def bisection(x1, x2):
    it = 1e-6                        # Accuracy needed
    def f(x): return( x - F - ecc * np.sin(x))
    while (abs(x2 - x1)/2.0 > it):   # Check the accuracy of mid point
        mid = (x1 + x2)/2            # Find the mid point
        if f(mid) == 0:              # if mid = 0 -> difintly the solution
            return(mid)              
        elif f(x1)*f(mid) < 0:       # Determine the sign to continue with upper or lower limit
            x2 = mid                 # if negative -> we are on the left
        else:
            x1 = mid                 # if positive -> we are on the right
    return(mid)


ecc = float(input("ecc: "))
F = float(input("F: "))
X = bisection(0, 2)
print("Bisection method    ", X)

#Ex[4.1.3] - Newton Raphson
#intial guess -> refine the value using differentiation(line slope) -> repeat

def nr(x):                                                            # Insert the first guess
    it = 1e-6                                                         # Accuracy needed      
    for i in range(100):                                              # No. of trials
        xnew = x - ((F - x + ecc*np.sin(x))/(ecc*np.cos(x) - 1))      # Apply on the formula (x - f'(x))
        if abs(xnew - x) < it: break                                  # Check the accuracy
        x = xnew                                                      # Reset x value
    print("Newton-Raphson   ",x)

ecc = float(input("ecc: "))
F = float(input("F: "))
x = nr(3)
