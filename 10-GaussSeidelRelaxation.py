#Ex[3.2] - Gauss - Seidel method with relaxation
#By start with a first guess of solution then uterate over it

def gauseirelax(A, B): 
    n = 1000
    l = len(A)
    x = [0.0, 0.0, 0.0] 
    w = 1.044769
    for k in range(0, n):                         # Number of trials
        for i in range(0, l):                     # length of the matrix
            ter = B[i]                           
            for j in range(0, l):      
                if(i != j):                                       # Ignore the diagonal terms
                    ter = (ter - A[i][j] * x[j]) + (1-w) * x[i]    # Apply on GS formula 
            x[i] = w * ter / A[i][i]                               # calculate x store the answer
    return x                                                      # return x to renew it with new iteration (more accurate)          

A = [[4.0, -1.0 , 1.0],[-1.0, 4.0, -2.0],[1.0, -2.0, 4.0]] 
B = [12.0, -1.0, 5.0] 
 
print(gauseirelax(A, B)) 
