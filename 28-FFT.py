# Ex[10.1] - Fourier Transform
# DFT is a transform operation between a discrete length N signal in the time domain to frequency domain
#  FFT is an algorithm for the efficient computation of DFT

import matplotlib.pyplot as plt
import numpy as np
from cmath import exp,pi
from numpy.fft import rfft,irfft
import copy

L = []
with open('sunspots.txt') as infile:   # Read the file
    for line in infile:                
        L.append(line.split()[1])      # Extract rhe second colomn
m=range(len(L))                        # create linespace from 0 to len(L)
L = [float(i) for i in L]              # Floating the values

plt.plot(m,L,"k-")
plt.xlabel("Time(month)",fontsize=10)
plt.ylabel("Sunspot No.",fontsize=10)
plt.xlim(0,3150)
plt.tight_layout()
plt.show()

"""
# DFT make kernel DIE
N1 = len(L)                                                
DFT = []

for k in range(N1//2+1):                                  # For each output element
    CK = np.zeros(N1//2+1,complex)                        # empty list to store the C_k cofficient (complex numbers) 
    for t in range(N1):                                   # For each input element
        CK = CK + L[t] * exp(-1 * 2j * pi * t * k / N1)   # Compute C_k from the formula
        DFT.append(CK)
print(DFT)
"""

# calculates Fast Fourier transform with FFT
N=len(L)                   
c=rfft(L)                             # Compute DFT for real values        
cabs=(abs(c))                         # take absolute values of C_k coefficient
kk=[]                                 # Store k values
maxc = 0                              # stor the maximum frequency

for i in range(N//2+1):               # Condition for any number of N (even or odd)
    if((cabs[i]>maxc) and (i>0)):     # check the c_k value is bigger than 
        maxc=cabs[i]                  # calculated the frequency corresponding to the max c_k
    kk.append(i)                      # store the counter (k)
    
plt.plot(kk,cabs,"b-")
plt.xlabel("$k$",fontsize=10)
plt.ylabel("$|c_k|$",fontsize=10)
plt.ylim(0,50000)
plt.xlim(0,1600)
plt.tight_layout()
plt.show()

# Reduce the Noice 
L2 = copy.copy(L)                           # Make a copy of Data list to modify the length
L2.remove(L2[3142])                         # Modify the length
p = range(len(L2))                          # Create linespace of new Data length

threshold = 3000                            # maximum value of c_k associated with noise
for k in range(N//2+1):
    if(cabs[k] < threshold):                # Make the cut
        c[k]=0.0
        
L3=irfft(c)                                 # FFT for real values

a,=plt.plot(m,L,"b-", label='Original signal')                       # with Noise
b,=plt.plot(p,L3,color="red",linewidth=2, label='De-Noised signal')   # without Noise
plt.xlabel("Time $t$",fontsize=10)
plt.ylabel("$f(t)$",fontsize=10)
plt.xlim(0,3000)
plt.ylim(0,300)
plt.tight_layout()
plt.legend()
plt.show()

