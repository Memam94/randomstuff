#Ex[8.1] - ODE
# Euler method - Using taylor expantion with approximation step h and ignoring the higher orders
# First order method   x(t+h) = x(t) + hf(x,t)
# Midpoint method - Evaluate the slope dx/dt at the interval midpoint h/2
# Second order method  x(t+h) = x(t) + k2    : k2 = hf(x+k1, t+h/2)  : k1 = h/2*f(x,t)

import numpy as np
from scipy import random
import matplotlib.pyplot as plt


def f(x,t): return ((-1)*(x)**(3) + np.sin(t))    # Define the function

def Eul(x0, t0, h, a, b, N):                      # Define Euler method ( x - dependant, t - independentant)
    trand = random.uniform(a,b,N)                 # Generate uniform N random numbers 
    xis, K = [], sorted(trand)
    for i in range(len(K)):                       
        if (t0 < K[i]):                           # Check if independant initial variable less than the random number
            x = 0                                 # initial dependant value
            x = x + h * f(x0,K[i])                # Apply Euler Formula -> update x value
            t0 = t0 + h                           # update t value
        xis.append(x)                             # Store x values acquired
    plt.plot(K,xis,'r-', label='Euler')

def mid(x0, t0, h, a, b, step):			          # Define Mid-Point method ( x - dependant, t - independentant)
    t = np.arange(a, b, step)			          # Time evolution list
    x = np.zeros(len(t))			              # Empty array to store x value
    for i in range(len(t)):
        k1 = (h/2.0) * f(x[i], t[i])		      # Apply on formula
        k2 = f(x[i] + k1, t[i] + h/2)
        x[i] = x[i] + h * k2
    plt.plot(t,x,'b-', label='Mid-point') 


Eul(0, 0, 0.4, 0, 100, 10000) 
mid(1, 0, 0.4, 0, 100, 1)

plt.legend()
plt.grid()

plt.show()