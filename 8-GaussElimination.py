#Ex[3.1] - Gauss Elimination

import numpy as np

A = np.array([[2,1.0,4,1.0,-4],[3,4,-1.0,-1.0,3],[1.0,-4,1.0,5,9],[2,-2,1,3,7]])

for i in range(0,4):                  # No. of rows = 4
    A[i,i:5] = A[i,i:5]/A[i,i]        # Apply on the colomns of each row by divide over the diagonal element (unity)  
    
    for k in range(0,4):                            # Run over colomns
        if k != i:                                  # Ignore the diagonal element 
            A[k,i:5] = A[k,i:5] - A[k,i]*A[i,i:5]   # Apply on the rows  for each colomn

# Back-Subistitution            
z = A[3,4]               
y = A[2,4] - A[2,3]*z
x = A[1,4] - A[1,2]*y - A[1,3]*z
w = A[0,4] - A[0,1]*x - A[0,2]*y - A[0,3]*z

print("w = ", round(w),"\nx = ",round(x),"\ny = ",y,"\nz = ",round(z))