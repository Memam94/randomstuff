#Ex[2.1.1] - Bubble sort
#Idea is comparing each pairs of numbers 
def bubsort(a):
    for j in range(len(a)-1,0,-1):                # define the range which start with pre-last number and take -1 step (swap backward)
        for i in range(j):                        # j runs from pre last number then decreasing up to the first number ends with index 0
            if a[i] > a[i+1]:                     # if the previous number is bigger
                a[i],a[i+1] = a[i+1],a[i]         # Swap
    print("This is a buble sorting",a)

a = [9, -3, 5, 2, 6, 8, -6, 1, 3]
bubsort(a)


#Ex[2.1.2] - Selection sort
#Idea is find and seperate the minimum each time
def selsort(a):
    so = []                                   #Empty list to store the minimum each time
    for i in range(0,len(a),1):               #The counter will act on whole list with step 1
        mi = a.index(min(a))                  #Find the index of the minimum number
        a[mi],a[0] = a[0], a[mi]               #Swap the min value with the first values
        so.append(a[0])                       #Insert the first value in the sorted list (so)
        a.remove(a[0])                        #Remove the first value from the original list then REPEAT
    print("This is selection sorting",so)
    
a = [9, -3, 5, 2, 6, 8, -6, 1, 3]
selsort(a)


#Ex[2.1.3] - Quick sort
#Idea is find the value of pivot and arrange the rest numbers into two sides then repeat for each
def quicksort(a):
    if len(a) > 1:                                #work as long the list contain more than one number
        pivot = a[0]                              #Choose the pivot as the first number
        i = 0                                     #initial value = 0 (before the first number)
        for j in range(len(a)-1):                 #the fisrt counter work over the list except the last element 
            if a[j+1] < pivot:                    #Check if the second number is less than pivot
                a[j+1],a[i+1] = a[i+1], a[j+1]    #if label the first no. i+1 (pivot) and swap it with j+1 no. (the very next)
                i += 1                            #move to next value of the devided list after 
        a[0],a[i] = a[i],a[0]                     #set the new pivot
        less = quicksort(a[:i])                   #sort the values before the namber which less than pivot with same function
        more = quicksort(a[i+1:])                 #sort the values after the namber which less than pivot with same function
        less.append(a[i])                         #insert the pivot in the less list
        return less + more                        #combine the two lists (bmore and less than pivot)
    else:
        return a                                    #until len(a) = 1 which have one element cannot be devided
    print("This is a quicksorting",a)

a = [9,-3,5,2,6,8,-6,1,3]
quicksort(a)
