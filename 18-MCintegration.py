#Ex[7.2] - MonteCarlo Integration
# using rejection method of random numbers
# Sum the random numbers accepted (located under the curve)

import numpy as np
from scipy import random
import matplotlib.pyplot as plt

a, b, c, I_tot = 0, 2, 1, []
N = [1000, 5000, 10000, 50000, 100000, 500000, 1000000, 5000000]

def f(x):                                    # Define the function
    return np.sin(1/x*(2-x))**2

for j in range(len(N)):                      # Choose number of random numbers generated
    xrand = random.uniform(a,b,N[j])         # first random number x
    yrand = random.uniform(a,c,N[j])         # second random number y
    accepted = [True if yrand[i] <= f(xrand[i]) else False for i in range(len(yrand))]
    
    # Shorten line of rejection method suggested by a friend
        #for i in range(len(yrand)):
        #if yrand[i] <= f(xrand[i]):
            #K.append((xrand[i] + yrand[i])**(0.5))
    #P.append(sum(K))
    #sums = np.diff(P)
    #sums = np.append(P[0],sums)  
    #I = ((b-a)/N[j])*sums[j]
    
    I = ((b-a)/N[j])*sum(accepted)           # Integration formula
    I_tot.append(I)                          # Store integration value for each N

plt.plot(N,I_tot,color='b',marker='o')
plt.xlabel('N', fontsize=10)
plt.xscale('log')
plt.ylabel('Integration value', fontsize=10)
plt.grid()

plt.show()