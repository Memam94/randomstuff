#Ex[3.3] - Current problem
#By start with a first guess of solution then uterate over it

def gausei(A, B): 
    n = 1000
    l = len(A)
    x = [0.0, 0.0, 0.0, 0.0] 
    
    for k in range(0, n):                         # Number of trials
        for j in range(0, l):                     # length of the matrix
            ter = B[j]                           
            for i in range(0, l):      
                if(j != i):                       # Ignore the diagonal terms
                    ter = ter - A[j][i] * x[i]    # Apply on GS formula 
            x[j] = ter / A[j][j]                  # calculate x store the answer
    return x                                      # return x to renew it with new iteration (more accurate)          

A = [[4, -1, -1, -1],[-1, 3, 0, -1],[-1, 0, 3, -1],[-1, -1, -1, 4]] 
B = [5, 0, 5, 0] 
 
print(gausei(A, B)) 
