#Ex[1.5] 
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import pylab

a = open("chirpmass_bin.dat","r")
b = open("tmerg_bin.dat","r")
c = open("chirpmass_tmerg_tot.dat","r").readlines()
M, T = [], []

for line in a:
    if not line.startswith('#'):
        M.append(float((line.strip())))

for line in b:
    if not line.startswith('#'):
        T.append(float((line.strip())))
        
with open("chirpmass_tmerg_tot.dat") as textFile:
    mat = [line.split() for line in textFile]

cs=plt.contourf(T, M, mat, cmap=cm.viridis)
cbar=plt.colorbar(cs,orientation='vertical')
pylab.xlim([0,14])
pylab.ylim([0,40])
cbar.solids.set_edgecolor("face")