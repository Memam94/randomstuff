#Ex[7.5] - Importance sampling Vs. Mean Value
# Same as mean value but use non-uniform random number instead of uniform ( mean method ) 
# Using weighted mean value instead of average

import numpy as np
import random
import matplotlib.pyplot as plt

N = [1000, 5000, 10000, 50000, 100000, 500000, 1000000, 5000000]
a, b = 0, 1
I_imp, I_mean = [], []

def f(x): return (x**(-0.5)) / (np.exp(x) + 1)
def w(x): return (x**(-0.5))

for j in range(0,len(N)):                    # IMPORTANCE SAMPLING
    sum = 0
    for k in range(int(N[j])):
        y = np.random.rand()                
        xrand = y**2                         # inverse sampling
        sum += f(xrand)/w(xrand)
    I = 2 * sum / N[j]                       # integration of w(x) = 2
    I_imp.append(I)

for j in range(0,len(N)):                    # MEAN VALUE SAMPLING 
    sum = 0
    for k in range(int(N[j])):
        xrand = np.random.rand() 
        sum += f(xrand)
    I = (b-a) * sum / N[j]
    I_mean.append(I)

plt.plot(N,I_imp,"r",marker='o', label='Importance sampling')
plt.plot(N,I_mean,"b",marker='*', label='Mean Value')
plt.xscale('log')
plt.xlabel('N',fontsize=10)
plt.ylabel('Integration',fontsize=10)
plt.legend()
plt.show()