#Ex[1.6] - Contour from file 

import matplotlib.pyplot as plt
import matplotlib.cm as cm
import numpy as np
import pylab

#Reading the file and store the colomns
f = open("time_BHillustris1_30.dat","r")
lines = f.readlines()
M1, M2 = [], []
for i in lines[1:]: 
    a = i.split() 
    M1.append(float(a[6]))
    M2.append(float(a[7]))

#swap and arrange heavy and light mass    
#for i in range(len(M2)):
#    if M1[i] > M2[i]:
#        M1[i], M2[i] = M2[i], M1[i]

#Define contour bins
dm, binx, biny = 10, np.zeros(6,float), np.zeros(6,float)
for i in range(1,len(binx)):
    binx[i] = binx[i-1] + dm
    biny[i] = biny[i-1] + dm

#create z matrix
index1, index2 = [], []
l = len(binx)
k = len(M1)
num = np.zeros([k,k],float)
for i in range(len(M1)):
    index1 = int(M1[i]/dm)
    index2 = int(M2[i]/dm)
    num[index1][index2] +=1

cs=plt.contourf(M1, M2, num, cmap=cm.viridis)
cbar=plt.colorbar(cs,orientation='vertical')
cbar.solids.set_edgecolor("face")