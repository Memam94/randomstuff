#Ex[6.2] - Box - Muller
# Generate two random numbers then apply cartisian-polar transformation

import numpy as np
from scipy.stats import norm
import matplotlib.pyplot as plt

sigma = 2

#create two random numbers
z1 = np.random.uniform(size=100000)       # First random number
z2 = np.random.uniform(size=100000)       # Second random number

#The Transformation
r = -2 * sigma * np.log(1-z1)             # radius
theta = 2 * np.pi * z2                    # angle

#x and y distribution
x = np.sqrt(r) * np.cos(theta)            # first x transformation
xs = sorted(x)
y = np.sqrt(r) * np.sin(theta)            # second y transformation
ys = sorted(y)

xnorm = norm.pdf(xs,0,2)                  # find pdf function for x
ynorm = norm.pdf(ys,0,2)                  # find pdf function for y

xgaus = np.random.normal(0, 2, 10000)               # Theoritical gaussian distribution

plt.plot(xs, xnorm, "r-", label='Theoritical Gaussian')                 # Plot the transformed random
plt.hist(xgaus, 100, density=True, label='Box-Muller');       # Plot theoritcal
plt.legend()
plt.grid()

plt.show()